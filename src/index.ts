import * as tmi from 'tmi.js';
import * as fs from 'fs';
import * as path from 'path';

import { Marbles } from './modules/marbles';
import { CustomPlayer } from './modules/customPlayer';
import { Witty } from './modules/witty';
import chalk from 'chalk';
import { Timer } from './modules/timer';

let bootsafe = true;
const configFile = './crasherbot-config.json';
const defaultConfig = {
	channel: 'TwitchChannelName',
	botname: 'CrasherBotty',
	oauth: 'oauth:XXX',
	home: 'C:\\Users\\Username\\',
	debugTwitch: true,
	modMarbles: true,
	modCustomPlayer: true,
	modWitty: true,
	modTimer: true,
};
if (!fs.existsSync(configFile)) {
	fs.writeFileSync(configFile, JSON.stringify(defaultConfig, null, 4));
	console.log(`Created config file ${configFile}, please update values`);
	bootsafe = false;
}
const config = Object.assign(
	{},
	defaultConfig,
	JSON.parse(fs.readFileSync(configFile, 'utf8')),
);
if (config.channel === defaultConfig.channel) {
	console.log(`Please update values in ${configFile}`);
	bootsafe = false;
}

process.env.homeDir = config.home as string;
if (!fs.existsSync(process.env.homeDir)) {
	console.log(`Could not find Home dir: ${process.env.homeDir}`);
	bootsafe = false;
}
process.env.appDataDir = path.join(config.home, 'AppData', 'Roaming');
if (!fs.existsSync(process.env.appDataDir)) {
	console.log(`Could not find AppData dir: ${process.env.appDataDir}`);
	bootsafe = false;
}
process.env.iRacingDir = path.join(process.env.homeDir, 'Documents', 'iRacing');
if (!fs.existsSync(process.env.iRacingDir)) {
	console.log(`Could not find iRacing dir: ${process.env.iRacingDir}`);
	bootsafe = false;
}

process.env.dataDir = path.join(process.env.appDataDir, 'CrasherBotty');
if (
	fs.existsSync(process.env.appDataDir) &&
	!fs.existsSync(process.env.dataDir)
) {
	console.log(`Creating Data dir: ${process.env.dataDir}`);
	fs.mkdirSync(process.env.dataDir);
}
process.env.playerDir = path.join(process.env.dataDir, 'players');
if (
	fs.existsSync(process.env.dataDir) &&
	!fs.existsSync(process.env.playerDir)
) {
	console.log(`Creating Player dir: ${process.env.playerDir}`);
	fs.mkdirSync(process.env.playerDir);
}

if (bootsafe) {
	fs.writeFileSync(configFile, JSON.stringify(config, null, 4));

	const client = new tmi.Client({
		options: { debug: config.debugTwitch, messagesLogLevel: 'info' },
		connection: {
			reconnect: true,
			secure: true,
		},
		identity: {
			username: config.botname,
			password: config.oauth,
		},
		channels: [config.channel],
	});

	if (config.modMarbles) {
		new Marbles(client);
	}

	if (config.modCustomPlayer) {
		new CustomPlayer(client);
	}

	if (config.modWitty) {
		new Witty(client);
	}

	if (config.modTimer) {
		new Timer(client);
	}

	client
		.connect()
		.then(() => {
			console.log(
				`${chalk.bgWhiteBright.black('Connected to chat')} ${
					config.channel
				}`,
			);
		})
		.catch((err) => {
			console.error(`${chalk.bgRed.black(err)}`);
		});
}
