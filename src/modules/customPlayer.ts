// @ts-ignore
import * as csscolors from 'css-color-names';

import { Player } from '../classes/player';
import { CrasherModule } from './crasherModule';

export class CustomPlayer extends CrasherModule {
	protected chat(command: string, player: Player, messageData: string[]) {
		if (!player.badges?.broadcaster) return;

		switch (command) {
			case 'name':
				if (messageData[1]) {
					player.data.driverName = messageData
						.join(' ')
						.replace(`${command} `, '');
					player.save();
				}
				return;

			case 'number':
				if (messageData[1]) {
					player.data.number = parseInt(messageData[1], 10);
					player.save();
				}
				return;

			// case 'color':
			// 	if (!messageData[1]) {
			// 		this.say(`@${player.data.twitchName}, please add a color`);
			// 		return;
			// 	}

			// 	if (!csscolors[messageData[1]]) {
			// 		this.say(
			// 			`@${player.data.twitchName}, we don't have that color`,
			// 		);
			// 		return;
			// 	}

			// 	player.data.color = csscolors[messageData[1]].replace('#', '');
			// 	player.save();
			// 	return;

			case 'irid':
				if (!messageData[1]) {
					this.say(
						`@${player.data.twitchName}, please add your iracing player ID`,
					);
					return;
				}

				if (isNaN(parseInt(messageData[1], 10))) {
					this.say(
						`@${player.data.twitchName}, That doesn't seem right`,
					);
					return;
				}

				player.data.irId = parseInt(messageData[1], 10);
				player.save();
				return;
		}

		return false;
	}
}
