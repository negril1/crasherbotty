import path from 'path';
import * as tmi from 'tmi.js';
import * as fs from 'fs';

import { Output } from '../interfaces';
import { firstNames, lastNames } from '../names';
import { CrasherModule } from './crasherModule';
import { Player } from '../classes/player';

export class Marbles extends CrasherModule {
	private isRegistering: boolean;
	private playerIds: string[];
	private rosterName: string;
	private maxPlayers: number;
	private aiRoster?: Output;

	constructor(client: tmi.Client) {
		super(client);

		this.isRegistering = false;
		this.playerIds = [];
		this.rosterName = '';
		this.maxPlayers = -1;
	}

	protected chatAdmin(command: string, messageData: string[]) {
		switch (command) {
			case 'marbles':
				this.start(messageData[1]);
				return true;
		}

		return false;
	}

	protected chat(command: string, player: Player) {
		switch (command) {
			case 'marbles':
				this.say(
					`Play marbles on iRacing! Wait untill the streamer opens registration and use !play to enter the race!`,
				);
				return;

			case 'play':
			case 'race':
				if (!this.isRegistering) {
					this.say(
						`Sorry @${player.data.twitchName}, there is no open registration`,
					);
					return;
				}

				const isAdded = this.addPlayer(player.data.twitchId);

				if (isAdded === 0) {
					this.say(
						`@${player.data.twitchName}, Sorry the grid is filled`,
					);
					return;
				}

				if (isAdded === -1) {
					this.say(
						`@${player.data.twitchName}, you can't enter twice`,
					);
					return;
				}
				return;
		}
	}

	private start(rosterName: string) {
		const channelName = this.channel!.replace('#', '');
		if (this.isRegistering) {
			this.say(`@${channelName}, we are already registering!`);
			return;
		}

		if (!rosterName) {
			this.say(`Please add the roster name: !marbles [roster]`);
			return;
		}

		const rosterPath = path.join(
			process.env.iRacingDir!,
			'airosters',
			rosterName,
			'roster.json',
		);
		if (!fs.existsSync(rosterPath)) {
			this.say(`@${channelName}, there is no such roster`);
			return;
		}
		this.aiRoster = JSON.parse(fs.readFileSync(rosterPath, 'utf8'));

		if (!this.aiRoster) {
			this.say(`Something went wrong fetching the AI roster`);
			return;
		}

		this.rosterName = rosterName;
		this.isRegistering = true;
		this.maxPlayers = this.aiRoster!.drivers.length;
		this.playerIds = [];

		this.say(
			`Starting marbles with ${this.aiRoster!.drivers.length} drivers`,
		);

		setTimeout(() => {
			this.say(`30 seconds left to enter`);
		}, 150 * 1000);

		setTimeout(() => {
			this.updateRoster();
		}, 180 * 1000);
	}

	private addPlayer(playerId: string) {
		if (this.playerIds.length < this.maxPlayers - 1) {
			if (this.playerIds.includes(playerId)) {
				return -1;
			}

			this.playerIds.push(playerId);
			return 1;
		}

		return 0;
	}

	private updateRoster() {
		if (!this.aiRoster) {
			this.say(`Something went wrong fetching the AI roster`);
			return;
		}

		let hasChatCar = false;

		this.aiRoster.drivers = this.aiRoster.drivers
			.map((driver) => {
				const driverData = {
					...driver,
				};

				const newDriverId = this.playerIds.pop();

				if (newDriverId) {
					const newDriver = Player.getPlayer(newDriverId);

					if (newDriver) {
						driverData.driverName = newDriver.driverName;
						driverData.carDesign = Player.parseColor(newDriver.carDesign);
						// if (newDriver.number) {
						// 	driverData.carNumber = newDriver.number;
						// }

						return driverData;
					}
				}

				if (!hasChatCar) {
					hasChatCar = true;
					driverData.driverName = 'Rest of chat';

					return driverData;
				}

				driverData.driverName = '[REMOVE]';

				// const first =
				// 	firstNames[Math.floor(Math.random() * firstNames.length)];
				// const last =
				// 	lastNames[Math.floor(Math.random() * lastNames.length)];

				// // TODO: This might create duplicate driver names
				// driverData.driverName = `${first} ${last}`;

				return driverData;
			})
			.filter((driverData) => {
				return driverData.driverName !== '[REMOVE]';
			});

		const rosterDir = path.join(
			process.env.iRacingDir!,
			'airosters',
			`${this.rosterName}_marbles`,
		);
		if (!fs.existsSync(rosterDir)) {
			fs.mkdirSync(rosterDir);
		}
		const rosterPath = path.join(rosterDir, 'roster.json');
		fs.writeFileSync(rosterPath, JSON.stringify(this.aiRoster, null, 4));

		this.say(
			`Registration closed. @${this.channel!.replace(
				'#',
				'',
			)}, please reload the iRacing UI`,
		);
		this.isRegistering = false;
		this.aiRoster = undefined;
	}
}
