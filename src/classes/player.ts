import * as tmi from 'tmi.js';
import * as fs from 'fs';
import * as path from 'path';
import { DesignData, Driver } from '../interfaces';

export class Player {
	public data: Driver;
	public badges?: tmi.Badges;
	public dataDir: string;
	public userId: string;
	public userFile: string;

	constructor(tags: tmi.ChatUserstate) {
		this.badges = tags.badges;
		this.userId = tags['user-id']!;
		this.dataDir = process.env.playerDir!;
		this.userFile = path.join(this.dataDir, this.userId);

		const color = tags.color?.replace('#', '');
		this.data = {
			driverName: tags['display-name']!,
			twitchName: tags['display-name']!,
			twitchId: this.userId,
			irId: -1,
			number: -1,
			carDesign: {
				designId: Math.floor(Math.random() * (8 - 1 + 1)) + 1,
				baseColor: color || '000000',
				accentColor: color || '000000',
				highLight: tags.subscriber ? 'FF0000' : 'FFFFFF',
			},
			suitDesign: {
				designId: 22,
				baseColor: '111111',
				accentColor: 'fc0706',
				highLight: 'ffffff',
			},
			helmetDesign: {
				designId: 22,
				baseColor: '111111',
				accentColor: 'fc0706',
				highLight: 'ffffff',
			},
			cache: {
				badges: this.badges,
			},
		};

		if (!fs.existsSync(this.userFile)) {
			console.log('writing');
			fs.writeFileSync(this.userFile, JSON.stringify(this.data, null, 4));
		}

		this.data = Object.assign(
			JSON.parse(fs.readFileSync(this.userFile, 'utf8')),
			this.data,
		);

		fs.writeFileSync(this.userFile, JSON.stringify(this.data, null, 4));
	}

	public getData() {
		return this.data;
	}

	public save() {
		fs.writeFileSync(this.userFile, JSON.stringify(this.data, null, 4));
	}

	public static getPlayer(id: string): Driver {
		const userFile = path.join(process.env.playerDir!, id);
		if (!fs.existsSync(userFile)) {
			console.log('This player doesnt exist yet?', id);
		}

		return JSON.parse(fs.readFileSync(userFile, 'utf8'));
	}

	public static parseColor(designData: DesignData): string {
		const {designId, baseColor, accentColor, highLight} = designData;

		return `${designId},${baseColor},${accentColor},${highLight},`;
	}
}
