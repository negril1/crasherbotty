import { DesignData, Driver, RaceOptions, OutputDriver } from "./interfaces";

// function parseDesign(design: DesignData, driver: Driver) {
//     return `${design.designId},${driver.color !== '' ? driver.color : design.baseColor},${design.accentColor},${design.highLight}`;
// }

// function outputDriver(driver: Driver, race: RaceOptions): OutputDriver {
//     return {
//         driverName: driver.driverName,
//         carDesign: parseDesign(driver.carDesign, driver),
//         carNumber: driver.number,
//         suitDesign: parseDesign(driver.suitDesign, driver),
//         helmetDesign: parseDesign(driver.helmetDesign, driver),
//         carPath: race.carPath,
//         carId: race.carId,
//         sponsor1: 0,
//         sponsor2: 0,
//         numberDesign: race.numberDesign,
//         driverSkill: 50,
//         driverAggression: 50,
//         driverOptimism: 50,
//         driverSmoothness: 50,
//         pitCrewSkill: 50,
//         strategyRiskiness: 50,
//         driverAge: 50,
//         id: '',
//         rowIndex: 0,
//         carClassId: 0,
//     }
// }